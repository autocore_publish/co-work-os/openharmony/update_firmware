import serial
import time
import _thread
import os
ret=os.popen("ls /dev/ttyU*")
ret=ret.read().split("\n")[:-1]
if len(ret)==0:
    print("没有找到可连接串口")
    exit(0)
elif len(ret)>1:
    for i in range(len(ret)):
        print(i,ret[i])
    p=int(input("输入编号:"))
else:p=0
ret=ret[p]
print("连接",ret)
ser = serial.Serial(ret, 115200, timeout=10000)#'/dev/ttyUSB0'
if ser.isOpen():
    print("--- 串口已打开 ---(退出按:q+enter)")
else:exit(0)

def read_and_print():
    while True:
        ss = input()+"\n"
        # print(ss)
        if ss == "q":
            ser.close()
            exit()
        ser.write(ss.encode("utf8"))

_thread.start_new_thread(read_and_print,())

while True:
    try:
        ret = ser.read()
        print(ret.decode("utf8"), end="")
    except:
        ser.close()
        exit(0)

ser.close()